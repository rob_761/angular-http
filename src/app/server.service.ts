import {Injectable} from "@angular/core";
import {Http, Headers, Response} from "@angular/http";
import 'rxjs/Rx';
import {Observable} from "rxjs/Observable"; //Observables

@Injectable()
export class ServerService {
  constructor(private http: Http){}

  storeServersPost(servers: any[]) {
    const headers = new Headers({'Content-Type': 'application/json'}); //defaults
   return this.http.post('https://udemy-ng-http-8c289.firebaseio.com/data.json', servers, {headers: headers}); //uses observables behind the scenes
  }

  storeServersPut(servers: any[]) {
    return this.http.put('https://udemy-ng-http-8c289.firebaseio.com/data.json', servers);
  }

  getServers() {
    return this.http.get('https://udemy-ng-http-8c289.firebaseio.com')
      .map((response: Response) => {
        const data = response.json();
        for (const server of data) {
          server.name = 'Fetched_' + server.name;
        }
        return data; //map wraps return to observable automatically
      })
      .catch(
        (error: Response) => {
          return Observable.throw('Something went wrong'); //catch doesn't wrap return automatically
        }
      );
  }

  getAppName() {
    return this.http.get('https://udemy-ng-http-8c289.firebaseio.com/data/appName.json')
      .map((response: Response) => {
        return response.json();
      });
  }

}
